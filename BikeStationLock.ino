#include<SoftwareSerial.h>
String code = "UNLOCK";
SoftwareSerial bluetooth(8,9);//RX,TX
int LEDPin = 5;
int powerPin = 6;
int lockPin = 4;
int poweredOnPeriod = 30000;//in ms
void setup() {
  // put your setup code here, to run once:
  pinMode(LEDPin,OUTPUT);
  pinMode(powerPin,OUTPUT);
  pinMode(lockPin,OUTPUT);
  digitalWrite(powerPin,HIGH);
  digitalWrite(LEDPin,HIGH);
  Serial.begin(9600);
  bluetooth.begin(9600);
}

void flushBluetooth()
{
  while(bluetooth.available())
    bluetooth.read();
}
void loop() {
  if(millis()>= poweredOnPeriod)
  {
    digitalWrite(powerPin,LOW);
  }
  // put your main code here, to run repeatedly:
  if(bluetooth.available()==6)
  {
    for(int i=0;i<6;i++)
    {
      if(code[i]!=bluetooth.read())
      {
        flushBluetooth();
        bluetooth.print("FAIL");
        return;
      }
    }
    digitalWrite(lockPin,HIGH);
    Serial.println("Passed!");
    bluetooth.print("PASS");
    digitalWrite(lockPin,HIGH);
    delay(500);
    digitalWrite(lockPin,LOW);  
  }
}
